<?php

class Parser
{
    public static $loader;

    const DOC_ROOT = "/var/www/parser/";

    public static function init()
    {
        if (self::$loader == null) {
            self::$loader = new self();
        }

        return self::$loader;
    }

    public function __construct()
    {
        spl_autoload_register(array($this, 'library'));
    }

    public function library($class)
    {
        set_include_path(self::DOC_ROOT . 'lib/');
        spl_autoload_extensions('.php');
        spl_autoload(strtolower($class));
    }

    public function amazonParser($search)
    {
        $product = array();
        // sending a request to Amazon and get an answer from him
        $data = file_get_contents(
            'http://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=' . urlencode($search)
        );
        $document = phpQuery::newDocument($data);
        //get the first element of result list
        $result = $document->find('.s-result-list li:first');
        //push info about product into array
        foreach ($result as $elements) {
            $product['price'] = pq($elements)->find('.s-price')->html();
            $product['title'] = pq($elements)->find('.s-access-title')->html();
            $product['image'] = pq($elements)->find('img')->attr('src');
        }
        if (empty($product)) {
            $noresult = $document->find('#noResultsTitle')->text();
            $product['noResult'] = $noresult;
        }
        print_r($product);

    }

    public function searchTable()
    {
        require_once('table.phtml');
    }
}

Parser::init();
if ($_SERVER['REQUEST_METHOD'] === 'GET' && !empty($_GET['keyword'])) {
    Parser::amazonParser($_GET['keyword']);
}
Parser::searchTable();


